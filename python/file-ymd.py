import requests

ip_address=str(input("Dime la IP: "))
 # Replace with the IP address you want to locate

# Use ipinfo.io to get geolocation information
response = requests.get(f"https://ipinfo.io/{ip_address}/json")
data = response.json()

print("IP Address Information:")
print("IP: ", data.get('ip'))
print("City: ", data.get('city'))
print("Region: ", data.get('region'))
print("Country: ", data.get('country'))
print("Location: ", data.get('loc'))