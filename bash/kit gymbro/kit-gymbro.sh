#!/bin/bash
# Author: Marcos Cordo Gutiérrez
# Licensed under GPLV3 or Higher
clear
usu=$(whoami)
quit="0"
rc=0
if ! [ $usu = "root" ]; then
    echo $usu
    echo -e "\e[91m [*] Tienes que ser root para poder jugar\e[39m"
    exit 1
fi
dpkg -l | grep -q "figlet" || rc=1
dpkg -l | grep -q "bc" || rc=1
for n in $(seq 1 3); do
    echo "Comprobando las dependencias."
    sleep 0.2
    clear
    echo "Comprobando las dependencias.."
    sleep 0.2
    clear
    echo "Comprobando las dependencias..."
    sleep 0.2
    clear
done
if [ $rc -eq 0 ]; then
    echo -e "\e[32m[*] Dependencias instaladas\e[39m"

else
    echo -e "\e[91m[*] Las dependecias no estan instaladas, las voy a instalar"
    apt update
    if [ $? -eq 0 ]; then
	apt install figlet bc
	    if [ $? -eq 0 ]; then
	        echo -e "\e[32m [*] Las dependecias han sido instaladas correctamente\e[0m"
	    fi
    fi
fi
sleep 2
clear
tit () {
    figlet "Gymbro kit"
}
menu1 () {
    echo "######################################"
    echo "#                                    #"
    echo -e "#       \e[33m1\e[39m --> Muestra el menú     "
    echo -e "#       \e[33m2\e[39m --> limpiar pantalla     "
    echo -e "#       \e[32m3\e[39m --> Calcular IMC           "
    echo -e "#       \e[32m4\e[39m --> Calcular Proteina           "
    echo -e "#       \e[32m5\e[39m --> Calcular Creatina           "
    echo -e "#       \e[32m6\e[39m --> Calcular kcal quemadas en reposo (dia)          "
    echo -e "#       \e[32m7\e[39m --> Calcular cantidad de calorias ingesta (dia)            "
    echo -e "#       \e[91m99\e[39m --> Salir      "
    echo "#                                    #"
    echo "######################################"
}
while [ $quit != "99" ]; do
    tit
    echo ""
    echo ""
    echo ""
    menu1
    echo ""
    echo ""
    read -p "¿Que quieres calcular mi gymbro?" quit
    if [ $quit = "1" ]; then
        tit
    elif [ $quit = "2" ]; then
        clear
        sleep 2
    elif [ $quit = "3" ]; then
        clear
        figlet "Calculadora de IMC"
        echo ""
        echo ""
        read -p "Dime tu peso en kg (si tiene decimal utiliza <<.>>)" peso
        echo ""
        read -p "Dime tu altura en m (si tiene decimal utiliza <<.>>)" altura
        echo ""
        resultado=$(bc -l <<< "$peso / ($altura*$altura)" | cut -d. -f1)
        if [ $resultado -lt 16 ]; then
            echo -e "Tu resultado es: Delgadez severa con \e[91m$resultado de IMC\e[39m"
        elif [ $resultado -ge 16 -a $resultado -lt 17 ]; then
            echo -e "Tu resultado es: Delgadez moderada con \e[33m$resultado de IMC\e[39m"
        elif [ $resultado -ge 17 -a $resultado -lt 18 ]; then
            echo -e "Tu resultado es: Delgadez aceptable con \e[32m$resultado de IMC\e[39m"
        elif [ $resultado -ge 18 -a $resultado -lt 25 ]; then
            echo -e "Tu resultado es: Peso Normal con \e[32m$resultado de IMC\e[39m"
        elif [ $resultado -ge 25 -a $resultado -lt 30 ]; then
            echo -e "Tu resultado es: Sobrepeso con \e[33m$resultado de IMC\e[39m"
        elif [ $resultado -ge 30 -a $resultado -lt 35 ]; then
            echo -e "Tu resultado es: Obeso Tipo 1 con \e[91m$resultado de IMC\e[39m"
        elif [ $resultado -ge 35 -a $resultado -le 40 ]; then
            echo -e "Tu resultado es: Obeso Tipo 2 con \e[91m$resultado de IMC\e[39m"
        elif [ $resultado -gt 40 ]; then
            echo -e "Tu resultado es: Obeso Tipo 3 con \e[91m$resultado de IMC\e[39m"
        fi
        echo ""
        echo "Pulse INTRO para seguir"; read
        echo ""
    elif [ $quit = "4" ]; then
        clear
        figlet "Calculadora de proteina diaria"
        echo ""
        echo ""
        read -p "Dime tu objetivo (mantener/definir/volumen)" prote
        echo ""
        read -p "Dime tu peso en kg (si tiene decimal utiliza <<.>>)" peso
        if [ $prote = "mantener" ]; then
            resultado=$(bc -l <<< "$peso*0.8")
            echo -e "La ingesta debe ser de \e[32m$resultado\e[39m gramos de proteina"
        elif [ $prote = "definir" ]; then
            resultado=$(bc -l <<< "$peso*2")
            echo -e "La ingesta debe ser de \e[32m$resultado\e[39m gramos de proteina"
        elif [ $prote = "volumen" ]; then
            resultado=$(bc -l <<< "$peso*2.2")
            echo -e "La ingesta debe ser de \e[32m$resultado\e[39m gramos de proteina"
        else
            echo -e "\e[91m [ERROR] Escribe bien las opciones\e[39m"
            sleep 0.5
        fi
        echo ""
        echo "Pulse INTRO para seguir"; read
        echo ""
    elif [ $quit = "5" ]; then
        clear
        figlet "Cantidad de Creatina"
        echo ""
        echo ""
        echo "El consumo de esta tiene dos fases"
        echo ""
        echo "##############################################################"
        echo "#                                                            #"
        echo "#                                                            #"
        echo -e "# \e[33mFase de carga (1º semana)\e[39m --> 1                            #"
        echo "#                                                            #"
        echo "#                                                            #"
        echo -e "# \e[33mFase de mantenimiento (2-3 meses)\e[39m --> 2                    #"
        echo "#                                                            #"
        echo "#                                                            #"
        echo "##############################################################"
        echo ""
        read -p "Elije tu fase (1/2)" fase
        if [ $fase = "1" ]; then
            echo ""
            echo "El consumo debe de ser de 20gm al dia repartido entre 4-5 raciones durante la primera semana"
        elif [ $fase = "2" ]; then
            echo ""
            echo "El consumo debe de ser de 5gm al dia durante 2-3 meses"
        else
            echo -e "\e[91m[ERROR] debes elegir una de las opciones indicadas\e[39m"
        fi
        echo ""
        echo "Pulse INTRO para seguir"; read
        echo ""
    elif [ $quit = "6" ]; then
        clear
        figlet "Kcal quemadas en reposo"
        echo ""
        echo ""
        read -p "Dime tu genero (hombre/mujer)" pre
        echo ""
        if [ $pre = "hombre" ]; then
            read -p "Dime tu peso (si hay decimal ponlo con <<.>>)" pre1
            read -p "Dime tu altura en cm" pre2
            read -p "Dime tu edad" pre3
            echo ""
            resultado=$(bc -l <<< "(10*$pre1)+(6.25*$pre2)-(5*pre3)+5")
            echo "Tus kcal son $resultado"
        elif [ $pre = "mujer" ]; then
            read -p "Dime tu peso (si hay decimal ponlo con <<.>>)" pre1
            read -p "Dime tu altura en cm" pre2
            read -p "Dime tu edad" pre3
            echo ""
            resultado=$(bc -l <<< "(10*$pre1)+(6.25*$pre2)-(5*pre3)-161")
            echo "Tus kcal son $resultado"
        else
            echo -e "\e[91m[ERROR] debes elegir una de las opciones indicadas\e[39m"
        fi
    #elif [ $quit = "7" ]; then

    fi

done
