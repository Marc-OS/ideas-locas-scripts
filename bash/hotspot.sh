#!/bin/bash
# Autor: Marcos Cordo Gutiérrez
# Licensed under GPLv3 Or Higher
# Fecha de creacion: mar 27 feb 2024 13:49:10 CET

# Función para mostrar el uso del script
show_usage() {
    echo "Uso: $0 [crear|modificar|eliminar]"
    echo "    crear       - Crea un nuevo punto de acceso"
    echo "    modificar   - Modifica la contraseña del punto de acceso existente"
    echo "    eliminar    - Elimina el punto de acceso"
}

# Función para crear un nuevo punto de acceso
crear_punto_acceso() {
    read -p "Introduce el nombre del SSID para el punto de acceso: " SSID
    read -s -p "Introduce la contraseña para el punto de acceso: " PASSWORD
    echo ""

    # Crear la conexión Wi-Fi
    nmcli connection add type wifi ifname '*' con-name $SSID autoconnect no ssid $SSID

    # Configurar la seguridad de la red Wi-Fi
    nmcli connection modify $SSID 802-11-wireless-security.key-mgmt wpa-psk
    nmcli connection modify $SSID 802-11-wireless-security.psk $PASSWORD

    # Habilitar el punto de acceso
    nmcli connection up $SSID
}

# Función para modificar la contraseña del punto de acceso
modificar_punto_acceso() {
    read -p "Introduce el nombre del SSID del punto de acceso que deseas modificar: " SSID
    read -s -p "Introduce la nueva contraseña para el punto de acceso: " PASSWORD
    echo ""

    # Modificar la contraseña del punto de acceso
    nmcli connection modify $SSID 802-11-wireless-security.psk $PASSWORD

    echo "Contraseña modificada correctamente para el punto de acceso: $SSID"
}

# Función para eliminar el punto de acceso
eliminar_punto_acceso() {
    read -p "Introduce el nombre del SSID del punto de acceso que deseas eliminar: " SSID

    # Eliminar el punto de acceso
    nmcli connection delete $SSID

    echo "Punto de acceso eliminado correctamente: $SSID"
}

# Comprobar el número de argumentos
if [ $# -ne 1 ]; then
    show_usage
    exit 1
fi

# Analizar el argumento
case $1 in
    crear)
        crear_punto_acceso
        ;;
    modificar)
        modificar_punto_acceso
        ;;
    eliminar)
        eliminar_punto_acceso
        ;;
    *)
        show_usage
        exit 1
        ;;
esac

exit 0
