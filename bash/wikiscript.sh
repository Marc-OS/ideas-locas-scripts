#!/bin/bash
# Autor: Marcos Cordo Gutiérrez
# Licensed under GPLv3 or Higher
if ! [ $# -eq 0 ]; then
    param=$1
    curl https://es.wikipedia.org/wiki/$param | grep jpg | head -n1 >> url.txt
    if [ $? -eq 0 ]; then
        cat url.txt | while read line; do
            meta=$(echo $line | cut -d"<" -f2 |  cut -d" " -f1)
            if [ $meta = "meta" ]; then
                url=$(echo $line | cut -d\" -f4)
                wget $url
                clear
            fi
        done
    fi
else
    echo -e "\e[91m[ ERROR ] Falta un argumento ej: ./wikiscript.sh <<parametro>>\e[39m"
fi
exit 0