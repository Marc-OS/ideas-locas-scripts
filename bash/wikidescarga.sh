#!/bin/bash

# Verifica que se proporcione un argumento
if [ $# -eq 0 ]; then
  echo "Uso: $0 <término de búsqueda>"
  exit 1
fi

# Término de búsqueda proporcionado como argumento
search_term="$1"

# URL de la API de Wikipedia para buscar imágenes
api_url="https://es.wikipedia.org/w/api.php?action=query&format=json&list=search&srsearch=${search_term}&srnamespace=6"

# Obtener la lista de resultados de búsqueda en formato JSON
search_results=$(curl -s "$api_url")

# Extraer las URL de las imágenes de los resultados
image_urls=$(echo "$search_results" | jq -r '.query.search[].title' | sed 's/ /_/g' | sed 's/^/https:\/\/es.wikipedia.org\/wiki\/File:/')

# Descargar las imágenes usando wget
for url in $image_urls; do
  # Extraer el nombre del archivo de la URL
  file_name=$(basename "$url")
  # Descargar la imagen
  wget -q "$url" -O "$file_name"
  echo "Descargada: $file_name"
done

echo "Descarga completa."

