#!/bin/bash
# Author: Marcos Cordo GUtiérrez
# email: mcordo2005@gmail.com
# Licensed under GPLv3 or HIgher
clear
titulo () {
    echo -e "\e[32m"
    echo "___  _ ____  _     _____  _     ____  _____   ____  ____  _      _     _____ ____  _____  _____ ____"
    echo "\  \///  _ \/ \ /\/__ __\/ \ /\/  _ \/  __/  /   _\/  _ \/ \  /|/ \ |\/  __//  __\/__ __\/  __//  __\ "
    echo " \  / | / \|| | ||  / \  | | ||| | //|  \    |  /  | / \|| |\ ||| | //|  \  |  \/|  / \  |  \  |  \/|"
    echo " / /  | \_/|| \_/|  | |  | \_/|| |_\\|  /_   |  \_ | \_/|| | \||| \// |  /_ |    /  | |  |  /_ |    /"
    echo "/_/   \____/\____/  \_/  \____/\____/\____\  \____/\____/\_/  \|\__/  \____\\_/\_\  \_/  \____\\_/\_\ "
    echo -e "\e[0m"
}
titulo
rc=0
r=0
ruta=$(pwd)
user=$(who | cut -d " " -f1 | head -n1)
programas () {
    apt install -y youtube-dl
}
echo ""
sleep 4
for n in $(seq 1 3); do
    echo "Searching dependences."
    sleep 0.3
    clear
    echo "Searching dependences.."
    sleep 0.3
    clear
    echo "Searching dependences..."
    sleep 0.3
    clear
done
dpkg -l | grep -iq "youtube-dl" || rc=1
if [ $rc -eq 0 ]; then
    echo -e "[*] \e[32mThe dependences are installed\e[39m"
    echo ""
    echo "To be continue..."
else
   echo -e " [*] \e[91mThe dependences isn't installed\e[39m"
   echo ""
   echo "I'm going to install them"
   programas
   if [ $? -eq 0 ]; then
	echo -e "\e[32m[*] All dependences installed\e[39m"
   else
	echo -e "\e[91m[ ERROR ] Error to install dependences\e[39m"
	exit 1
   fi
fi
clear
titulo
if [ $# -eq 2 ]; then
    if [ $1 = "-mp3" ]; then
        for n in $(seq 1 3); do
            echo "Searching audio in Youtube."
            sleep 0.3
            clear
            echo "Searching audio in Youtube.."
            sleep 0.3
            clear
            echo "Searching audio in Youtube..."
            sleep 0.3
            clear
        done
        youtube-dl -x --audio-format mp3 $2
	if [ $? -eq 0 ]; then
	    xdg-open $ruta
	else
	    echo "\e[91m[ ERROR ] Dowloading interrumped\e[39m"
	    exit 1
	fi
    fi
    if [ $1 = "-mp4" ]; then
	for n in $(seq 1 3); do
    	    echo "Searching video in Youtube."
    	    sleep 0.3
    	    clear
    	    echo "Searching video in Youtube.."
    	    sleep 0.3
    	    clear
    	    echo "Searching video in Youtube..."
    	    sleep 0.3
    	    clear
	done
	youtube-dl --recode-video mp4 $2
	if [ $? -eq 0 ]; then
            xdg-open $ruta
	else
	    echo "\e[91m[*] Dowloading interrumped\e[39m"
	fi
    fi
else
    echo -e "\e[91m[ Error ] Introduce two arguments\e[39m"
    echo ""
    echo "Example: ./youtube_converter.sh -mp3/mp4 <<URL>>"
    exit 1
fi
exit 0
