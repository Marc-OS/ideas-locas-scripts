#!/bin/bash
usu=$(whoami)
rc=0
if ! [ $usu = "root" ]; then
    echo -e "\e[91m[ ERROR ] No eres superusuario\e[39m"
    exit 1
fi
dpkg -l | grep -iq "figlet" || rc=1
if [ $rc -ne 0 ]; then
    apt update && apt install figlet -y
fi
if ! [ -f ./passw.txt ]; then
    touch passw.txt 
fi
figlet "Password Generator"
echo ""
echo ""
# Funcion para generar una passwd aleatoria
read -p "Indica la longitud: " word
echo ""
read -p "indica el tamaño en (bytes) de la wordlist: " tamano
generate_password() {
    local length=$word
    local password=$(tr -dc 'A-Za-z0-9!@#$%^&*()_+<>?=' </dev/urandom | fold -w ${length} | head -n1)
    echo ${password}
}
ls -l | grep passw.txt | tr -s " " | cut -d " " -f5 | while read line; do
    if [ $line -ge $tamano ]; then
        exit 1 
    fi
    generate_password > passw.txt
done
