#!/bin/bash
# Author: Marcos Cordo Gutiérrez
# email: mcordo2005@gmail.com
# Licensed under GPLv3 or Higher
ruleta="0"
# Función para generar un número aleatorio entre 1 y 36
user=$(whoami)
rc=0
count=0
clear
tit () {
    echo "######################################"
    echo "#                                    #"
    echo -e "#       \e[33mhelp\e[39m --> Muestra el menú     #"
    echo -e "#       \e[32mok\e[39m --> Otra tirada           #"
    echo -e "#       \e[91mstop\e[39m --> Parar de jugar      #"
    echo "#    (si te quedas sin coins paras)  #"
    echo "#                                    #"
    echo "######################################"
}
if ! [ $user = "root" ]; then
    echo -e "\e[91m [*] Tienes que ser root para poder jugar\e[39m"
    exit 1
fi
dpkg -l | grep -q "figlet" || rc=1
for n in $(seq 1 3); do
    echo "Comprobando las dependencias."
    sleep 0.2
    clear
    echo "Comprobando las dependencias.."
    sleep 0.2
    clear
    echo "Comprobando las dependencias..."
    sleep 0.2
    clear
done
if [ $rc -eq 0 ]; then
    echo -e "\e[32m[*] Dependencias instaladas\e[39m"

else
    echo -e "\e[91m[*] Las dependecias no estan instaladas, las voy a instalar"
    apt update
    if [ $? -eq 0 ]; then
	apt install figlet
	if [ $? -eq 0 ]; then
	    echo -e "\e[32m [*] Las dependecias han sido instaladas correctamente"
	fi
    fi
fi
sleep 3
clear
figlet "Ruleta Casinera"
echo ""
tit
echo ""
while [ $ruleta != "stop" ]; do 
        read -p "Dime que quieres hacer: " ruleta
    if [ $ruleta = "ok" ]; then
    read -p "Dime un número: " numero
    read -p "Dime un color (rojo/negro/verde): " color
    echo ""
    echo "######################################################"
	random_number() {
	    echo $((RANDOM % 36))
        }
	# Generar un número aleatorio y almacenarlo en una variable
	winning_number=$(random_number)
        # Mostrar el número ganador
        echo "Tocó el número $winning_number!"
        # Mostrar si el número es rojo o negro
        if [[ $winning_number -eq 0 ]]; then
	    # El 0 es verde
            color1="verde"
            echo -e "\e[32mEl número es verde!\e[39m"
        elif [[ $winning_number -le 10 && $winning_number%2 -eq 0 ]] || [[ $winning_number -ge 19 && $winning_number%2 -eq 0 ]]; then
            # Los números pares entre 1 y 10, y entre 19 y 36 son rojos
            color1="rojo"
            echo -e "\e[91mEl número es rojo!\e[39m"
        else
            color1="negro"
            # Los números impares entre 1 y 10, y entre 19 y 36 son negros
            echo "El número es negro!"
        fi
        if [[ $winning_number = $numero ]] && [[ $color = $color1 ]]; then
            echo -e "\e[32mHas ganado!!!\e[39m"
        else
            echo -e "\e[91mHas perdido!!!\e[39m"
        fi
        echo "######################################################"
        echo ""
	((count ++))

    elif [ $ruleta = "help" ]; then
    clear
	tit	
    elif [ $ruleta = "stop" ]; then
    	nada=0
    else
	echo -e "\e[91m [ ERROR ] Escribe bien!!! Ej: <<ok/stop/help>>\e[39m"
    fi
done
echo "Resumen: "
echo ""
echo -e "El total de tiradas ha sido <<\e[32m$count\e[39m>>"
exit 0
