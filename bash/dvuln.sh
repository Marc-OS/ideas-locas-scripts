#!/bin/bash
# Autor: Marcos Cordo
# Licensed under GPLv3 or Higher
# Este script va a detectar por parametros los equipos y vulnerabilidades de ellos de los que esten en tu RLO.
clear
usu=$(whoami)
quit="0"
rc=0
if ! [ $usu = "root" ]; then

    echo -e "\e[91m [*] Tienes que ser root para poder ejecutar\e[39m"
    exit 1
fi
dpkg -l | grep -q "figlet" || rc=1
dpkg -l | grep -q "nmap" || rc=1
dpkg -l | grep -q "iputils-ping" || rc=1
for n in $(seq 1 3); do
    echo "Comprobando las dependencias."
    sleep 0.2
    clear
    echo "Comprobando las dependencias.."
    sleep 0.2
    clear
    echo "Comprobando las dependencias..."
    sleep 0.2
    clear
done
if [ $rc -eq 0 ]; then
    echo -e "\e[32m[*] Dependencias instaladas\e[39m"

else
    echo -e "\e[91m[*] Las dependecias no estan instaladas, las voy a instalar"
    apt update
    if [ $? -eq 0 ]; then
        apt install figlet nmap iputils-ping -y
            if [ $? -eq 0 ]; then
                echo -e "\e[32m [*] Las dependecias han sido instaladas correctamente\e[0m"
            fi
    fi
fi
sleep 2
clear
tit () {
    echo -e "\e[32m"; figlet "Vuln_script_utils"; echo -e "\e[39m"
}
menu1 () {
    echo "#######################################"
    echo "#                                     #"
    echo -e "#       \e[33m1\e[39m --> Muestra el menú         #"
    echo -e "#       \e[33m2\e[39m --> limpiar pantalla        #"
    echo -e "#       \e[32m3\e[39m --> Detectar ip suelta      #"
    echo -e "#       \e[32m4\e[39m --> Escaneo de red          #"
    echo -e "#       \e[32m5\e[39m --> Detectar vuln de una ip #           "
    echo -e "#       \e[32m6\e[39m --> modo desatendido        #"
    echo -e "#       \e[91m99\e[39m --> Salir                  #"
    echo "#                                     #"
    echo "#######################################"
}
while [ $quit != 99 ]; do
    tit
    echo ""
    echo ""
    echo ""
    menu1
    echo ""
    echo ""
    read -p "Dime lo que deseas: " quit
    if [ $quit -eq 1 ]; then
        echo ""
        sleep 1
        menu1
    elif [ $quit -eq 2 ]; then
        clear
        sleep 1
        menu1
    elif [ $quit -eq 3 ]; then
        clear
        sleep 2
        read -p "Dime la ip a la que quieres detectar: " ip
        read -p "¿Quieres que se vuelque a un fichero? (si/no) " file
        if [ $file = "si" ]; then
            if [ -f $ruta/escaneo.txt ]; then
                rm $ruta/escaneo.txt
            fi
            nmap $ip > escaneo.txt
            ruta=$(pwd)
            if [ $? -eq 0 ]; then
                thunar $ruta
            fi
        elif [ $file = "no" ]; then
            nmap $ip
        else
            echo -e "\e[91m[ ERROR ] Escribe bien!!\e[39m"
            exit 1
        fi
    elif [ $quit -eq 4 ]; then
        clear
        sleep 2
        read -p "Dime el nombre de la interfaz: " inter
        read -p "¿Quieres que se vuelque a un fichero? (si/no) " file
        if [ $file = "si" ]; then
            if [ -f $ruta/escaneo.txt ]; then
                rm $ruta/escaneo.txt
            fi
            ip=$(ip a | grep -w $inter | grep -w "inet" | tr -s " " | cut -d " " -f3)
            nmap -sP $ip > escaneo.txt
            ruta=$(pwd)
            if [ $? -eq 0 ]; then
                thunar $ruta
            fi
        elif [ $file = "no" ]; then
            ip=$(ip a | grep -w $inter | grep -w "inet" | tr -s " " | cut -d " " -f3)
            nmap -sP $ip
        else
            echo -e "\e[91m[ ERROR ] Escribe bien!!\e[39m"
            exit 1
        fi
    elif [ $quit -eq 5 ]; then
        clear
        sleep 2
        read -p "Dime la ip que quieres detectar vulns: " vuln
        read -p "¿Quieres que lo vuelque a un fichero? (si/no) " file
        if [ $file = "si" ]; then
            if [ -f $ruta/vuln.txt ]; then
                rm $ruta/vuln.txt
            fi
            nmap --script vuln $vuln > vuln.txt
            if [ $? -eq 0 ]; then
                thunar $ruta
            fi
        elif [ $file = "no" ]; then
            nmap --script vuln $vuln
        else
            echo -e "\e[91m[ ERROR ] Escribe bien!!"
        fi
    elif [ $quit -eq 6 ]; then
        inter=$(ip a | grep "2:" | tr -s " " | cut -d " " -f2 | cut -d: -f1)
        ip=$(ip a | grep -w $inter | grep -w "inet" | tr -s " " | cut -d " " -f3)
        nmap -sP $ip > desatendido.txt
        if [ $? -eq 0 ]; then
            grep -oE '\b([0-9]{1,3}\.){3}[0-9]{1,3}\b' "desatendido.txt" | awk '!a[$0]++' > ips.txt
            if [ $? -eq 0 ]; then
                mkdir ./vuln
                cat ips.txt | while read line; do
                    nmap --script vuln $line > ./vuln/$line.txt
                    if [ $? -eq 0 ]; then
                        echo "" &
                    fi 
                done
            fi
        fi
    elif [ $quit -eq 99 ]; then
        echo "" &
    else
        echo -e "\e[91m[ ERROR ] Escribe bien!!\e[39m"
        exit 1
    fi
done
exit 0