#!/bin/bash
# Author: Marcos Cordo Gutiérrez
# Licensed under GPLv3 or Higher
connections=$(netstat -ano | grep "ESTABLISHED" || netstat -ano | grep "ESTABLECIDO")
echo "Servicio  IP      Puerto"
while read -r line; do
        ip=$(echo $line | awk '{print $5}' | awk -F: '{print $1}')
        port=$(echo $line | awk '{print $5}' | awk -F: '{print $2}')
        service=$(echo $line | awk '{print $8}')
        echo "$service  $ip     $port"
        myIP=$(hostname -I | cut -d " " -f1)
        if [ $myIP != $ip ]; then
                echo "Obteniendo informacion de la ip...."
                response=$(curl -s "https://ipinfo.io/$ip")
                echo $response
        fi
done <<< "$connections"
exit 0